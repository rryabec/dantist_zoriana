'use strict';

export default class PageError {
  constructor() {
    this.name = 'page-error';
    console.log('%s module', this.name.toLowerCase());
  }
}
