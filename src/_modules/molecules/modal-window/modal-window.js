'use strict';

import jqueryValidation from 'jquery-validation'
export default class ModalWindow {
  constructor() {
    let self = this;
    // this.modalSelector = '.js-modal';    
    // this.modalOverlay = '.js-modal-overlay';
    this.modal = '.js-modal-form';
    // this.btnOpen = '.js-modal-open';
    // this.btnClose = '.js-modal-cross';

    this.initOrder();

    // console.log('%s module', this.orderSelector.toLowerCase());
  }
  initOrder() {
    let self = this;
    let captchaImg = $(this.modal).find('.captcha').addClass('modal-window__captcha-img');
    let captchaInput = $(this.modal).find('#id_captcha_1').addClass('field__input');
    let captchaLabel = $(this.modal).find('.field__label-captcha, #id_captcha_1').wrapAll("<div class='field modal-window__field-captcha'></div>");
    let captchaInputHidden = $(this.modal).find('.captcha, .modal-window__field-captcha, #id_captcha_0').wrapAll("<div class='modal-window__captcha'></div>");
    $(captchaImg).click(() => {
      this.refrechCaptcha();
    });
    $(document).ready(function () {
      function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
          let cookies = document.cookie.split(';');
          for (let i = 0; i < cookies.length; i++) {
            let cookie = $.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === name + '=') {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
            }
          }
        }
        return cookieValue;
      }
      $(self.modal).validate({
        rules: {
          fullName: {
            required: true
          },
          mail: {
            email: true,
            required: true
          },
          message: {
            required: true
          }
        },
        messages: {
          fullName: {
            required: "Введіть ім'я"
          },
          mail: {
            email: "Неправельний формат email адреса",
            required: "Введіть email aдрес"
          },
          message: {
            required: "Введіть повідомлення"
          }
        },
        onclick: true,
        errorClass: "error",
        validClass: "valid",
        highlight: function (element, errorClass, validClass) {
          $(element).parent().children().addClass('error').removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).parent().children().removeClass('error').addClass(validClass);
        }
      });

      $(self.modal).submit(function (e) {
        e.preventDefault();
        let Name = $('#fullName').val();
        let email = $('#mail').val();
        let textArea = $('#message').val();
        var that = $(this);
        if ($(this).valid()) {
          $.ajax({
            url: "/",
            method: that.attr('method'),
            data: $(this).serialize(),
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            headers: {
              'X-CSRFToken': getCookie('csrftoken'),
            },
            success: function (response) {
              if (response.message == "Invalid captcha") {
                $('img.captcha').trigger('click');
              } else {
                self.cleanModal();
              }
            }
          })
        }
      });
    });
  }
  refrechCaptcha() {
    let self = this;
    let url = location.protocol + "//" + window.location.hostname + ":" + location.port + "/captcha/refresh/";
    $.getJSON(url, {}, function (json) {
      $(self.modal).find('#id_captcha_0').val(json.key);
      $(self.modal).find('.captcha').attr('src', json.image_url);
      $(self.modal).find('#id_captcha_1').val('');
    });
    return false;
  }
  // closeModal(){
  //   let self= this;
  //   $(this.modal).removeClass('open');

  //   setTimeout(function(){
  //     $(self.modalSelector).removeClass('open');
  //   },2);
  // }
  cleanModal() {
    $(this.modal).fadeOut();
    $('.main__diagnosis .main__title').fadeOut();
    $('.main__diagnosis-text').fadeIn(500);
  }
  // closeModal(){
  //   let self= this;


  // }

}