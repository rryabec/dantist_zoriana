'use strict';
import { TimelineLite, Power4 } from 'gsap';
export default class Services {
  constructor() {
    let self = this;
    // this.blockServices = '.services'
    // this.items = $('.services__items').children();
    this.item = '.services__item';

    // this.tl = new TimelineLite();
    // this.waypoint = new Waypoint({
    //   element: $(this.blockServices).get(0),
    //   offset: '75%',
    //   handler: function (direction) {
    //     alert('hello')
    //     self.tl.staggerTo(self.items, 1, {
    //       ease: Power4.easeInOut,
    //       y: 0,
    //       opacity: 1,
    //     }, .1)
    //   },
    // });
    this.initServices();
  }
  initServices() {
    let list = $(this.item).find('ul').addClass('services__list')
    $(list).children().addClass('services__list-item')
  }
}
